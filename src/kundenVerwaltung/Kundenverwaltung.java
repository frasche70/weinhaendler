package kundenVerwaltung;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import kunde.AKunde;
import kunde.Adresse;
import kunde.Endverbraucher;
import kunde.GesellschaftMLiz;
import kunde.Grossverbraucher;
import kunde.KundenNummernVergeber;

public class Kundenverwaltung implements Comparator<AKunde> {

	static final String kundenFile = "kunden.csv";
	protected static LinkedList<AKunde> kundenListe = new LinkedList<AKunde>();
	

	public Kundenverwaltung() {
		readKundenfile(kundenFile);
	}
	
	public static List<AKunde> getKunden() {
//		String[] kunden = new String[kundenListe.size()];
//		int cnt = 0;
//		for (Iterator<AKunde> iterator = kundenListe.iterator(); iterator.hasNext();) {
//			AKunde aKunde = (AKunde) iterator.next();
//			kunden[cnt] = aKunde.toString();
//			cnt++;
//		}
		return kundenListe;
	}
	

	public void saveKunden() {
		String csvStr = "";
		for (Iterator<AKunde> iterator = kundenListe.iterator(); iterator.hasNext();) {
			AKunde aKunde = (AKunde) iterator.next();

			String typ = aKunde.getClass().getSimpleName();
			if (typ.equals("Endverbraucher")) {
				csvStr += typ + ";" + aKunde.getNummer() + ";" + aKunde.getAnschrift().getCSVString() + ";"
						+ aKunde.getRabatt() + ";" + (((Endverbraucher) aKunde).isBonitaet() ? "T" : "F") + ";;";
			}
			if (typ.equals("GesellschaftMLiz")) {
				csvStr += typ + ";" + aKunde.getNummer() + ";" + aKunde.getAnschrift().getCSVString() + ";"
						+ aKunde.getRabatt() + ";;" + ((GesellschaftMLiz) aKunde).getVertrieb();
			}
			if (typ.equals("Grossverbraucher")) {
				csvStr += typ + ";" + aKunde.getNummer() + ";" + aKunde.getAnschrift().getCSVString() + ";"
						+ aKunde.getRabatt() + ";;" + ((Grossverbraucher) aKunde).getStatus();
			}
			csvStr += System.getProperty("line.separator");
		}
		saveKundenfile(csvStr, kundenFile);
	}

	public void addKunde(AKunde kunde) {
		kundenListe.add(kunde);
	}

	public void removeKunde(AKunde kunde) {
		kundenListe.remove(kunde);
	}

	public void neuEndverbraucher(boolean bonitaet) {
		AKunde akunde = new Endverbraucher(0.0, bonitaet);
		akunde.setNummer(this.getNewNummer());
		addKunde(akunde);
	}

	public void neuGesellschaftMLiz(int vertrieb) {
		AKunde akunde = new GesellschaftMLiz(7.0, vertrieb);
		akunde.setNummer(this.getNewNummer());
		addKunde(akunde);
	}

	public void neuGrossverbraucher(int status) {
		AKunde akunde = new Grossverbraucher(5.0, status);
		akunde.setNummer(this.getNewNummer());
		addKunde(akunde);
	}

	public int compare(AKunde kunde1, AKunde kunde2) {
		return kunde1.getNummer().compareTo(kunde2.getNummer());
	}

	public void saveKundenfile(String csvStr, String filename) {
		FileWriter fw;
		try {
			fw = new FileWriter(filename);
			fw.write(csvStr);
			fw.flush();
			fw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	private String getNewNummer() {
		Boolean found = false;
		String newNummer = "";
		while (true) {
			KundenNummernVergeber knv = KundenNummernVergeber.getInstance();
			newNummer = knv.getKundenNummer();
			for (AKunde aKunde : kundenListe) {
				if(aKunde.getNummer().equals(newNummer)) {
					found = true;
				}
			}
			if(!found) {
				break;
			}
		}
		return newNummer;
	}

	public static void readKundenfile(String filename) {
		System.out.println("readKundenfile");
		double rabatt = 0.0;
		boolean bonitaet = false;
		int vertrieb = 0;
		int status = 0;
		try {
			BufferedReader FileReader = new BufferedReader(new java.io.FileReader(new File(filename)));
			String zeile = "";
			while (null != (zeile = FileReader.readLine())) {
				String parts[] = zeile.split(";");
				String typ = parts[0];
				String nummer = parts[1];
				Adresse adr = new Adresse(parts[2], parts[3], parts[4], parts[5]);
				rabatt = Double.valueOf(parts[6]);
				if (typ.equals("Endverbraucher")) {
					bonitaet = false;
					if (parts[7].equals("T")) {
						bonitaet = true;
					}
					Endverbraucher endV = new Endverbraucher(rabatt, bonitaet);
					endV.setNummer(nummer);
					endV.setAnschrift(adr);
					kundenListe.add(endV);
				}
				if (typ.equals("GesellschaftMLiz")) {
					vertrieb = Integer.valueOf(parts[8]);
					GesellschaftMLiz gmL = new GesellschaftMLiz(rabatt, vertrieb);
					gmL.setNummer(nummer);
					gmL.setAnschrift(adr);
					kundenListe.add(gmL);
				}
				if (typ.equals("Grossverbraucher")) {
					status = Integer.valueOf(parts[8]);
					Grossverbraucher grossV = new Grossverbraucher(rabatt, status);
					grossV.setNummer(nummer);
					grossV.setAnschrift(adr);
					kundenListe.add(grossV);
				}
			}
			FileReader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		return "Kundenverwaltung [kundenListe=" + kundenListe + "]";
	}

}
