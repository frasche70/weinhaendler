package bestellung;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import model.WeinsortenEnum;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.util.Locale;

public class BestellungEingabe extends JFrame {
	private JPanel contentPane;
	private JTextField tfAnzahl;
	private JTextField tfPreis;
	private JTextField tfGesamtPreis;
	private JComboBox weinSorteBox;
	private ButtonGroup group;
	private JRadioButton jRadioButton1, jRadioButton2, jRadioButton3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		BestellungEingabe frame = new BestellungEingabe();
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	public BestellungEingabe() {
		// Panel
		setTitle("Eingabe");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		// Weinsorte
		String[] werte = new String[] { WeinsortenEnum.CHARDONAY.name(), WeinsortenEnum.CHIANTI.name(),
				WeinsortenEnum.RIESLING.name() };
		weinSorteBox = new JComboBox<String>(werte);
		weinSorteBox.setSize(120, 20);
		weinSorteBox.setLocation(150, 110);
		weinSorteBox.setEditable(true);
		weinSorteBox.addActionListener(new Auswahl());
		contentPane.add(weinSorteBox);
		JLabel lblWeinsorte = new JLabel("Weinsorte");
		lblWeinsorte.setBounds(80, 110, 150, 20);
		contentPane.add(lblWeinsorte);

		// Anzahl
		JLabel lblAnzahl = new JLabel("Anzahl");
		lblAnzahl.setBounds(80, 140, 150, 20);
		contentPane.add(lblAnzahl);
		tfAnzahl = new JTextField();
		tfAnzahl.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {//||e.getKeyCode() == KeyEvent.VK_TAB) {
					int anzahl;
					double preis;
					anzahl = Integer.parseInt(tfAnzahl.getText());
				 preis = Double.parseDouble(tfPreis.getText().replace(',', '.'));
					 preis=anzahl*preis;
					 DecimalFormat f = new DecimalFormat("#0.00");
					tfGesamtPreis.setText((preis+""));
				}
			}
		});
		tfAnzahl.setBounds(150, 140, 120, 20);
		contentPane.add(tfAnzahl);
		
		// Preis pro Stuck
		JLabel lblPreis = new JLabel("Preis");
		lblPreis.setBounds(80, 170, 150, 20);
		contentPane.add(lblPreis);
		tfPreis = new JTextField();
		tfPreis.setEditable(false);
		tfPreis.setBounds(150, 170, 120, 20);
		DecimalFormat f = new DecimalFormat("#0.00");
		tfPreis.setText(f.format(WeinsortenEnum.CHARDONAY.getPreis()));
		contentPane.add(tfPreis);

		// Gesamtpreis
		JLabel lblGesamtPreis = new JLabel("Gesamtpreis");
		lblGesamtPreis.setBounds(80, 200, 150, 20);
		contentPane.add(lblGesamtPreis);
		tfGesamtPreis = new JTextField();
		tfGesamtPreis.setEditable(false);
		tfGesamtPreis.setBounds(150, 200, 120, 20);
		tfGesamtPreis.setText(f.format(0));
		contentPane.add(tfGesamtPreis);

		// Flaschen,Palette,Karton
		jRadioButton1 = new JRadioButton("Palette");
		jRadioButton2 = new JRadioButton("Karton");
		jRadioButton3 = new JRadioButton("Flasche(n)");
		group = new ButtonGroup();
		group.add(jRadioButton1);
		group.add(jRadioButton2);
		group.add(jRadioButton3);
		jRadioButton1.setBounds(80, 10, 100, 20);
		jRadioButton2.setBounds(80, 40, 100, 20);
		jRadioButton3.setBounds(80, 70, 100, 20);
		contentPane.add(jRadioButton1);
		contentPane.add(jRadioButton2);
		contentPane.add(jRadioButton3);
	}

	class Auswahl implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String wert = "";
			JComboBox item = (JComboBox) e.getSource();
			if (item.getSelectedIndex() > -1) {
				// Es wurde ein fertiger Wert gew�hlt
				wert = (String) item.getSelectedItem();
			} else {
				// Jemand hat eine eigene Eingabe gemacht
				wert = item.getEditor().getItem().toString();
			}
			DecimalFormat f = new DecimalFormat("#0.00");
			if (wert.equals(WeinsortenEnum.CHIANTI.name())) {
				tfPreis.setText(f.format(WeinsortenEnum.CHIANTI.getPreis()));
			} else if (wert.equalsIgnoreCase(WeinsortenEnum.RIESLING.name())) {
				tfPreis.setText(f.format(WeinsortenEnum.RIESLING.getPreis()));
			} else {
				tfPreis.setText(f.format(WeinsortenEnum.CHARDONAY.getPreis()));
			}
		}
	}

}
