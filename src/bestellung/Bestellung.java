package bestellung;


import java.util.Date;

import model.WeinSorte;


public class Bestellung implements Comparable<Bestellung>{
	private int anzahl;
	private Date datum;
	private double gesamtPreis;
	private double gewaehrterRabatt = 0.0;
	private double gesamtPreisNetto;
	private WeinSorte wein;
	
	public Bestellung(int anzahl, WeinSorte wein) {
		this.anzahl = anzahl;
		this.wein = wein;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public double getGesamtPreis() {
		return gesamtPreis;
	}

	public void setGesamtPreis(double gesamtPreis) {
		this.gesamtPreis = gesamtPreis;
	}

	public double getGewaehrterRabatt() {
		return gewaehrterRabatt;
	}

	public void setGewaehrterRabatt(double gewaehrterRabatt) {
		this.gewaehrterRabatt = gewaehrterRabatt;
	}

	public double getGesamtPreisNetto() {
		return gesamtPreisNetto;
	}

	public void setGesamtPreisNetto(double gesamtPreisNetto) {
		this.gesamtPreisNetto = gesamtPreisNetto;
	}

	public WeinSorte getWein() {
		return wein;
	}

	public void setWein(WeinSorte wein) {
		this.wein = wein;
	}

	@Override
	public String toString() {
		return "Bestellung [anzahl=" + anzahl + ", datum=" + datum + ", gesamtPreis=" + gesamtPreis
				+ ", gewaehrterRabatt=" + gewaehrterRabatt + ", gesamtPreisNetto=" + gesamtPreisNetto + ", wein=" + wein
				+ "]";
	}
	
	public void printTest() {
		System.out.println("Anzahl von Flaschen: " + anzahl);
	}

	@Override
	public int compareTo(Bestellung o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
