package gui;

import java.awt.BorderLayout; 
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.border.LineBorder;

import gui.kundenGui.PanelBestellung;
import gui.kundenGui.PanelKundenArt;
import gui.kundenGui.PanelKundenBild;
import gui.kundenGui.PanelKundenListe;
import kundenVerwaltung.Kundenverwaltung;

import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.JList;

public class Weinvertrieb extends JFrame {
	
	private final ButtonGroup buttonGroup = new ButtonGroup();
	public static Kundenverwaltung kv = new Kundenverwaltung();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Weinvertrieb frame = new Weinvertrieb();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Weinvertrieb() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 500);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu Datei = new JMenu("Datei");
		Datei.setFont(new Font("Arial", Font.BOLD, 15));
		menuBar.add(Datei);
		JMenuItem mntmNeu = new JMenuItem("neu");
		Datei.add(mntmNeu);
		// add(menuBar);

		JMenu Bestellung = new JMenu("Bestellung");
		Bestellung.setFont(new Font("Arial", Font.BOLD, 15));
		menuBar.add(Bestellung);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Anlegen");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		Bestellung.add(mntmNewMenuItem_2);
		
		JMenuItem mntmNewMenuItem_3 = new JMenuItem("\u00C4ndern");
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		Bestellung.add(mntmNewMenuItem_3);
		
		JMenuItem mntmNewMenuItem_4 = new JMenuItem("L\u00F6schen");
		mntmNewMenuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		Bestellung.add(mntmNewMenuItem_4);
		// add(menuBar);

		JMenu Kunde = new JMenu("Kunde");
		Kunde.setFont(new Font("Arial", Font.BOLD, 15));
		menuBar.add(Kunde);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("\u00C4ndern");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		JMenuItem mntmAnlegen = new JMenuItem("Anlegen");
		Kunde.add(mntmAnlegen);
		Kunde.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("L\u00F6schen");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		Kunde.add(mntmNewMenuItem_1);
		// add(menuBar);

		JMenu Hilfe = new JMenu("Hilfe");
		Hilfe.setFont(new Font("Arial", Font.BOLD, 15));
		menuBar.add(Hilfe);
		
		JMenuItem mntmNewMenuItem_5 = new JMenuItem("Not Avaible");
		Hilfe.add(mntmNewMenuItem_5);
		getContentPane().setLayout(new GridLayout(2, 1, 0, 0));
		
		JPanel panelKunde = new JPanel();
		getContentPane().add(panelKunde);
		
		JPanel panelKundenListe = new PanelKundenListe();
		panelKunde.add(panelKundenListe);
		
		JPanel panelBild = new PanelKundenBild();
		panelKunde.add(panelBild);
		
		JPanel panelKundenArt = new PanelKundenArt();
		panelKunde.add(panelKundenArt);
		
		JPanel panelBestellung = new PanelBestellung();
		getContentPane().add(panelBestellung);
	}
}
