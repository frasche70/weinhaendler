package gui.kundenGui;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.FlowLayout;
import java.awt.GridLayout;

public class PanelKundeAnlegen extends JPanel{
	
	// Endverbraucher = 0%
	// Grossverbraucher = 5%
	// GesellschaftMLiz = 7%
	
	private JTextField name, vorname, strasse, plz, ort;
	public PanelKundeAnlegen() {
		setLayout(new GridLayout(0, 2, 0, 0));
		
		JPanel panelKontaktDaten = new JPanel();
		add(panelKontaktDaten);
		
		// Kontaktdaten
		Box kontaktDatenBox = Box.createVerticalBox();
		panelKontaktDaten.add(kontaktDatenBox);
		kontaktDatenBox.setBorder(BorderFactory.createTitledBorder("Adressdaten"));
		name = new JTextField();
		vorname = new JTextField();
		strasse = new JTextField();
		plz = new JTextField();
		ort = new JTextField();		
		
		vorname.setColumns(10);
		name.setColumns(10);
		strasse.setColumns(10);
		plz.setColumns(10);
		ort.setColumns(10);
		
		kontaktDatenBox.add(name);
		kontaktDatenBox.add(vorname);
		kontaktDatenBox.add(strasse);
		kontaktDatenBox.add(plz);
		kontaktDatenBox.add(ort);
		
		// Kundenart
		JPanel panelKundenArt = new JPanel();
		add(panelKundenArt);
		
		Box kundenArtBox = Box.createVerticalBox();
		panelKundenArt.add(kundenArtBox);
		JRadioButton endkunde = new JRadioButton("Endkunde");
		JRadioButton grossverbraucher = new JRadioButton("Grossverbraucher");
		JRadioButton gesellschaftMLiz = new JRadioButton("Gesellschaft mit Liz.");
		ButtonGroup gruppeKundenArt = new ButtonGroup();
		gruppeKundenArt.add(endkunde);
		gruppeKundenArt.add(grossverbraucher);
		gruppeKundenArt.add(gesellschaftMLiz);
		
		kundenArtBox.add(endkunde);
		kundenArtBox.add(grossverbraucher);
		kundenArtBox.add(gesellschaftMLiz);
	}

}
