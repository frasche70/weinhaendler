package gui.kundenGui;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import gui.kundenGui.KundeFrame;

public class Kunde {

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame frame = new KundeFrame("Kundenverwaltung");
				frame.setSize(500, 400);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);

			}
		});


	}

}
