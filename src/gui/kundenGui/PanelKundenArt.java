package gui.kundenGui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

public class PanelKundenArt extends JPanel {
	static JRadioButton rdbtnEndverbraucher;
	static JRadioButton rdbtnGrossverbraucher;
	static JRadioButton rdbtnGesellschaftMLiz;
	ButtonGroup kArt;
	
	class RadioAktionen implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (rdbtnEndverbraucher.isSelected()) {
				//Macht im Moment noch nichts!
			}
		}
	}

	public PanelKundenArt() {

		rdbtnEndverbraucher = new JRadioButton("Endverbraucher");
		rdbtnEndverbraucher.setHorizontalAlignment(SwingConstants.LEFT);

		rdbtnGrossverbraucher = new JRadioButton("Grossverbraucher");
		rdbtnGrossverbraucher.setHorizontalAlignment(SwingConstants.LEFT);

		rdbtnGesellschaftMLiz = new JRadioButton("Gesellschaft mit Liz.");
		rdbtnGesellschaftMLiz.setHorizontalAlignment(SwingConstants.LEFT);
		setLayout(new GridLayout(0, 1, 0, 0));
		kArt = new ButtonGroup();

		kArt.add(rdbtnEndverbraucher);
		kArt.add(rdbtnGrossverbraucher);
		kArt.add(rdbtnGesellschaftMLiz);

		add(rdbtnEndverbraucher);
		add(rdbtnGrossverbraucher);
		add(rdbtnGesellschaftMLiz);

		rdbtnEndverbraucher.addActionListener(new RadioAktionen() );
	}
}
