package gui.bestellungGui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.event.EventListenerList;;

public class DetailPanel extends JPanel {

	public JTextField tfGesamtPreis;
	private EventListenerList listenerList = new EventListenerList();
	public JTextField tfPreis;
	public JComboBox<String> weinSorteBox;
	private String verpackung;

	public DetailPanel() {
		Dimension size = getPreferredSize();
		size.width = 250;

		setPreferredSize(size);

		setBorder(BorderFactory.createTitledBorder("Eingabe"));

		// Flasche,Palette,Karton
		JRadioButton jRadioButton1 = new JRadioButton("Palette");
		JRadioButton jRadioButton2 = new JRadioButton("Karton");
		JRadioButton jRadioButton3 = new JRadioButton("Flasche(n)");
		jRadioButton1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				verpackung = jRadioButton1.getText();// "Palette";
			}
		});
		jRadioButton2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				verpackung = jRadioButton2.getText();// "Karton";
			}
		});
		jRadioButton3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				verpackung = jRadioButton3.getText();//"Flasche";
			}
		});
		ButtonGroup group = new ButtonGroup();
		group.add(jRadioButton1);
		group.add(jRadioButton2);
		group.add(jRadioButton3);
		Box verticalBox = Box.createVerticalBox();
		verticalBox.add(jRadioButton1);
		verticalBox.add(jRadioButton2);
		verticalBox.add(jRadioButton3);

		// Weinsort
		String[] werte = new String[] { WeinsortenEnum.CHARDONEY.name(), WeinsortenEnum.CHIANTI.name(),
				WeinsortenEnum.RIESLING.name() };
		weinSorteBox = new JComboBox<String>(werte);
		weinSorteBox.setSize(120, 20);
		weinSorteBox.setLocation(150, 110);
		weinSorteBox.setEditable(true);
		weinSorteBox.addActionListener(new Auswahl());

		JLabel lblWeinsorte = new JLabel("Weinsorte");
		JLabel lblAnzahl = new JLabel("Anzahl: ");
		JLabel lblPreis = new JLabel("Preis: ");

		final JTextField tfAnzahl = new JTextField(10);
		tfAnzahl.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					int anzahl;
					double preis;
					anzahl = Integer.parseInt(tfAnzahl.getText());
					preis = Double.parseDouble(tfPreis.getText().replace(',', '.'));
					if (verpackung.equals("Palette")) {
						anzahl = 36 * anzahl;
					} else if (verpackung.equals("Karton")) {
						anzahl = 12 * anzahl;
					} else
						anzahl = anzahl;
					preis = anzahl * preis;
					DecimalFormat f = new DecimalFormat("#0.00");
					tfGesamtPreis.setText(f.format(preis));

				}
			}
		});
		tfPreis = new JTextField(10);
		tfPreis.setEditable(false);
		DecimalFormat f = new DecimalFormat("#0.00");
		tfPreis.setText(f.format(WeinsortenEnum.CHARDONEY.getPreis()));

		// Gesamtpreis
		JLabel lblGesamtPreis = new JLabel("Gesamtpreis");
		tfGesamtPreis = new JTextField(10);
		tfGesamtPreis.setEditable(false);
		tfGesamtPreis.setText(f.format(0));

		JButton addBtn = new JButton("Add");
		addBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String anzahl = tfAnzahl.getText();
				String preis = tfPreis.getText();
				String gesamtPreis = tfGesamtPreis.getText();
				String weinsorte = weinSorteBox.getEditor().getItem().toString();
				//Object[] columnNames = { "Nr", "Anzahl", "Verpackung", "Weinsorte", "Einzel-Preis", "Brutto", "Netto",
			//"Rabat" };
				Object[] bestell1= {1,anzahl,verpackung,weinsorte,preis, gesamtPreis,gesamtPreis,0};
			//	String text = "Bestellt: " + anzahl + " " + verpackung + weinsorte + " f�r " + preis + " Euro. Gesamt "
			//			+ gesamtPreis + " Euro-\n";
				fireDetailEvent(new DetailEvent(this, bestell1));
				DecimalFormat f = new DecimalFormat("#0");
				tfAnzahl.setText(f.format(0));
			}
		});

		setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();

		// First column
		gc.weightx = 0.5;
		gc.weighty = 0.5;
		gc.gridx = 0;
		gc.gridy = 0;
		add(verticalBox, gc);

		gc.anchor = GridBagConstraints.LINE_END;
		gc.gridx = 0;
		gc.gridy = 1;
		add(lblWeinsorte, gc);

		gc.gridx = 0;
		gc.gridy = 2;
		add(lblAnzahl, gc);

		gc.gridx = 0;
		gc.gridy = 3;
		add(lblPreis, gc);

		gc.gridx = 0;
		gc.gridy = 4;
		add(lblGesamtPreis, gc);

		gc.anchor = GridBagConstraints.LINE_START;
		gc.gridx = 1;
		gc.gridy = 1;
		add(weinSorteBox, gc);

		gc.gridx = 1;
		gc.gridy = 2;
		add(tfAnzahl, gc);

		gc.gridx = 1;
		gc.gridy = 3;
		add(tfPreis, gc);

		gc.gridx = 1;
		gc.gridy = 4;
		add(tfGesamtPreis, gc);

		// Final row
		gc.weighty = 10;
		gc.anchor = GridBagConstraints.LAST_LINE_START;
		gc.gridx = 1;
		gc.gridy = 5;
		add(addBtn, gc);

	}

	public void fireDetailEvent(DetailEvent event) {
		Object[] listeners = listenerList.getListenerList();

		for (int i = 0; i < listeners.length; i += 2) {
			if (listeners[i] == DetailListener.class) {
				((DetailListener) listeners[i + 1]).detailEventOccurred(event);
			}
		}
	}

	public void addDetailListener(DetailListener listener) {
		listenerList.add(DetailListener.class, listener);
	}

	public void removeDetailListener(DetailListener listener) {
		listenerList.remove(DetailListener.class, listener);
	}

	class Auswahl implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String wert = "";
			JComboBox item = (JComboBox) e.getSource();
			if (item.getSelectedIndex() > -1) {
				// Es wurde ein fertiger Wert gew�hlt
				wert = (String) item.getSelectedItem();
			} else {
				// Jemand hat eine eigene Eingabe gemacht
				wert = item.getEditor().getItem().toString();
			}
			DecimalFormat f = new DecimalFormat("#0.00");
			if (wert.equals(WeinsortenEnum.CHIANTI.name())) {
				tfPreis.setText(f.format(WeinsortenEnum.CHIANTI.getPreis()));
			} else if (wert.equalsIgnoreCase(WeinsortenEnum.RIESLING.name())) {
				tfPreis.setText(f.format(WeinsortenEnum.RIESLING.getPreis()));
			} else {
				tfPreis.setText(f.format(WeinsortenEnum.CHARDONEY.getPreis()));
			}
		}
	}

}
