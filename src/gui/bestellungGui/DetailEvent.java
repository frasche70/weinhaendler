package gui.bestellungGui;

import java.util.EventObject;

public class DetailEvent extends EventObject {

	private Object[] bestel;

	public DetailEvent(Object source, Object[] bestel) {
		super(source);

		this.bestel = bestel;
	}

	public Object[] getBestel() {
		return bestel;
	}

	

	
}
