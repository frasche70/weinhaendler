package gui.bestellungGui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

public class MainFrame extends JFrame {

	private DetailPanel detailsPanel;
	private BestellungPanel tabBestellung;
	
	public MainFrame(String title) {
		super(title);

		// Set layout manager
		setLayout(new BorderLayout());

		// Create Swing component
		final JTextArea textArea = new JTextArea();

		detailsPanel = new DetailPanel();
		 detailsPanel.addDetailListener(new DetailListener(){
			 public void detailEventOccurred(DetailEvent event){
				 Object[] bestel1=event.getBestel();
				// textArea.append(text);
				 DefaultTableModel model = (DefaultTableModel) tabBestellung.getModel();
		        model.addRow(bestel1);
			 }
		 	});
		
		
		// Add Swing components to content pane
		Container c = getContentPane();
		tabBestellung=new BestellungPanel();
		tabBestellung.setSize(500,100);
		DefaultTableModel model = (DefaultTableModel) tabBestellung.getModel();
		c.add(textArea, BorderLayout.SOUTH);
		c.add(tabBestellung, BorderLayout.CENTER);
		c.add(detailsPanel, BorderLayout.WEST);

	}

}
