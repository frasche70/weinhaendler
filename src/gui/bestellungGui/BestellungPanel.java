package gui.bestellungGui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import javafx.scene.control.TableRow;

public class BestellungPanel extends JPanel {
	final JTable tabBestellung;
	public BestellungPanel() {
//		
//		Dimension size = getPreferredSize();
//		size.width = 500;
//		//setPreferredSize(new Dimension(510, 322));

	
		setBorder(BorderFactory.createTitledBorder("Bestellung"));

		Object[] columnNames = { "Nr", "Anzahl", "Verpackung", "Weinsorte", "Einzel-Preis", "Brutto", "Netto",
				"Rabat" };
		tabBestellung = new JTable(new DefaultTableModel(null, columnNames));
		tabBestellung.setAutoResizeMode(4);
//		TableColumn column = null;
//		for (int i = 0; i < 8; i++) {
//			column = tabBestellung.getColumnModel().getColumn(i);
//			column.setPreferredWidth(50);
//
//		}
		
//		//tabBestellung.setBounds(0, 0, 500, 100);
		JScrollPane scroll = new JScrollPane(tabBestellung);
		//scroll.setSize(100,10);
		// scroll.setBounds(0, 0, 50, 10);
       // tabBestellung.setSize(500, 200);
        tabBestellung.setAutoResizeMode(4);
		setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();
		gc.anchor = GridBagConstraints.FIRST_LINE_START;
		gc.weighty = 1;
		gc.gridx = 0;
		gc.gridy = 0;
		add(scroll, gc);
			

		
	}

	public DefaultTableModel getModel() {
		return (DefaultTableModel) tabBestellung.getModel();
	}
}
