package gui.bestellungGui;

public enum WeinsortenEnum {
	RIESLING(3), CHARDONEY(7), CHIANTI(5);
	
	private double preis;
	//Constructor
	
	WeinsortenEnum(double preis) {
		this.setPreis(preis);
	}
	
	//Getters and Setters
	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}
}
