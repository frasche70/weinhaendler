package kunde;

public class AKunde {
	protected String nummer; //lt.Doku doch ein String
	protected Adresse anschrift;
	protected Double rabatt;
	
	AKunde(Double rabatt){
		if(rabatt == null) {
			if(this instanceof Endverbraucher) {
				rabatt = 0.0;
			}else if(this instanceof Grossverbraucher) {
				rabatt = 5.0;
			}else if(this instanceof GesellschaftMLiz) {
				rabatt = 7.0;
			}else {
				rabatt = 0.0;
			}
		}
		this.rabatt = rabatt;
	}

	public String getNummer() {
		return nummer;
	}

	public void setNummer(String nummer) {
		this.nummer = nummer;
	}

	public Adresse getAnschrift() {
		return anschrift;
	}

	public void setAnschrift(Adresse anschrift) {
		this.anschrift = anschrift;
	}

	public Double getRabatt() {
		return rabatt;
	}

	public void setRabatt(Double rabatt) {
		this.rabatt = rabatt;
	}
	
	public String getCSVAnschriftString() {
		return anschrift.getCSVString();
	}
	public String toString() {
		return "AKunde [nummer=" + nummer + ", rabatt=" + rabatt + "]";
	}
	
	
}
