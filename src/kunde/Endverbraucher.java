package kunde;

public class Endverbraucher extends AKunde {
	protected boolean bonitaet;
	
	public Endverbraucher(Double rabatt, boolean bonitaet) {
		super(rabatt);
		this.bonitaet = bonitaet;
	}

	public boolean isBonitaet() {
		return bonitaet;
	}

	public void setBonitaet(boolean bonitaet) {
		this.bonitaet = bonitaet;
	}

	@Override
	public String toString() {
		return "Endverbraucher: Bonit�t: " + bonitaet + " Kunden-Nr: "+ getNummer() + " Rabatt: " + getRabatt() + "% Anschrift: " + getAnschrift();
	}
	
}
