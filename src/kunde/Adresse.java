package kunde;

public class Adresse {
	
	private String name;
	private String strasse_hnr;
	private String plz;
	private String ort;
	
	
	public Adresse(String name, String strasse_hnr, String plz, String ort) {
		super();
		this.name = name;
		this.strasse_hnr = strasse_hnr;
		this.plz = plz;
		this.ort = ort;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStrasse_hnr() {
		return strasse_hnr;
	}

	public void setStrasse_hnr(String strasse_hnr) {
		this.strasse_hnr = strasse_hnr;
	}

	public String getPlz() {
		return plz;
	}

	public void setPlz(String plz) {
		this.plz = plz;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}
	public String getCSVString() {
		return this.name +";" + this.strasse_hnr + ";" + this.plz+ ";" + this.ort; 
	}
	
	@Override
	public String toString() {
		return "" + this.name + " " + this.strasse_hnr + " " + this.plz + " " + this.ort;
	}
}
