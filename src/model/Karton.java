package model;

public class Karton extends AVerpackungseinheit {

	// Constructors
	public Karton() {
		super(0);
		System.out.println("Muss man implementieren");
	}

	public Karton(int anzahlFlaschen) {
		super(anzahlFlaschen);
	}

	// toString
	@Override
	public String toString() {
		return "Karton";
	}

}
