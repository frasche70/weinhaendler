package model;

public class AVerpackungseinheit {
	protected int anzahlFlaschen = 1;

	// Constructor
	public AVerpackungseinheit(int anzahlFlaschen) {
		this.anzahlFlaschen = anzahlFlaschen;
	}

	// Getters und Setters
	public int getAnzahlFlaschen() {
		return anzahlFlaschen;
	}

	public void setAnzahlFlaschen(int anzahlFlaschen) {
		this.anzahlFlaschen = anzahlFlaschen;
	}

	// toString
	@Override
	public String toString() {
		return "anzahlFlaschen: " + anzahlFlaschen;
	}
}
